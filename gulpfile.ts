export {}
const gulp = require('gulp')
const ts = require('gulp-typescript')
const typescript = require('typescript')

const svelte = require('gulp-svelte')

const rollup = require('rollup')
const rollupTypescript = require('@rollup/plugin-typescript')
const rollupResolve = require('@rollup/plugin-node-resolve').nodeResolve
const rollupSvelte = require('rollup-plugin-svelte')
const sveltePreprocess = require('svelte-preprocess')

const path = require('path')

/**
 * TypeScript transformers
 * @returns {typescript.TransformerFactory<typescript.SourceFile>}
 */
function createTransformer() {
  /**
   * @param {typescript.Node} node
   */
  function shouldMutateModuleSpecifier(node) {
    if (
      !typescript.isImportDeclaration(node) &&
      !typescript.isExportDeclaration(node)
    )
      return false
    if (node.moduleSpecifier === undefined) return false
    if (!typescript.isStringLiteral(node.moduleSpecifier)) return false
    if (
      !node.moduleSpecifier.text.startsWith('./') &&
      !node.moduleSpecifier.text.startsWith('../')
    )
      return false
    if (path.extname(node.moduleSpecifier.text) !== '') return false
    return true
  }

  /**
   * Transforms import/export declarations to append `.js` extension
   * @param {typescript.TransformationContext} context
   */
  function importTransformer(context) {
    return (node) => {
      /**
       * @param {typescript.Node} node
       */
      function visitor(node) {
        if (shouldMutateModuleSpecifier(node)) {
          if (typescript.isImportDeclaration(node)) {
            const newModuleSpecifier = typescript.createLiteral(
              `${node.moduleSpecifier.text}.js`
            )
            return typescript.updateImportDeclaration(
              node,
              node.decorators,
              node.modifiers,
              node.importClause,
              newModuleSpecifier
            )
          } else if (typescript.isExportDeclaration(node)) {
            const newModuleSpecifier = typescript.createLiteral(
              `${node.moduleSpecifier.text}.js`
            )
            return typescript.updateExportDeclaration(
              node,
              node.decorators,
              node.modifiers,
              node.exportClause,
              newModuleSpecifier
            )
          }
        }
        return typescript.visitEachChild(node, visitor, context)
      }

      return typescript.visitNode(node, visitor)
    }
  }

  return importTransformer
}

const tsConfig = ts.createProject('tsconfig.json', {
  getCustomTransformers: (_program) => ({
    after: [createTransformer()],
  }),
})

const options = {
  outDir: path.resolve('dist'),
  manifest: './src/module.json',
  tsEntrypoint: './src/module/importer.ts',
}

let rollupCache
async function buildTS() {
  const bundle = await rollup.rollup({
    input: options.tsEntrypoint,
    cache: rollupCache,
    preserveEntrySignatures: 'allow-extension',
    plugins: [
      rollupTypescript(),
      rollupResolve(),
      rollupSvelte({
        preprocess: sveltePreprocess(),
      }),
    ],
    manualChunks(id) {
      if (id.includes('node_modules')) {
        return 'vendor'
      }
    },
  })

  rollupCache = bundle.cache

  await bundle.write({
    dir: './dist',
    format: 'es',
    preferConst: true,
    minifyInternalExports: false,
    exports: 'auto',
    chunkFileNames: '[name].js',
    sourcemap: true,
  })
}

gulp.task('svelte', () => {
  return gulp.src('src/**/*.svelte').pipe(svelte()).pipe(gulp.dest('dist/'))
})

gulp.task('svelte', buildTS)

gulp.task('compile', () => {
  return gulp.src('src/**/*.ts').pipe(tsConfig()).pipe(gulp.dest('dist/'))
})

gulp.task('copy', async () => {
  return new Promise<void>((resolve, reject) => {
    gulp.src('README.md').pipe(gulp.dest('dist/'))
    gulp.src('src/module.json').pipe(gulp.dest('dist/'))
    gulp.src('src/lang/**').pipe(gulp.dest('dist/lang/'))
    gulp.src('src/templates/**').pipe(gulp.dest('dist/templates/'))
    gulp.src('src/styles/**').pipe(gulp.dest('dist/styles/'))
    gulp.src('src/assets/**').pipe(gulp.dest('dist/assets/'))
    resolve()
  })
})

gulp.task('build', gulp.parallel('compile', 'copy'))

gulp.task('watch', function () {
  gulp.watch('src/**/*', gulp.series('build'))
})

/*
// This is supposed to copy the dist folder into the modules directory for testing. Only works if you've set it up the right way
//This works if development path is FoundryVTT/Data/dev/modules/swade-item-macros
const MODULEPATH = "../../../modules/swade-item-macros/"
gulp.task('foundry', () => {
  return gulp.src('dist/**').pipe(gulp.dest(MODULEPATH))
})
gulp.task("update", gulp.series('build', 'foundry'))
*/
