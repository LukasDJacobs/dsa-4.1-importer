declare global {
  class DSAImporterGame extends Game {
    importer: () => void
  }
}

export function getGame(): DSAImporterGame {
  if (!(game instanceof Game)) {
    throw new Error('game is not initialized yet!')
  }
  return game as DSAImporterGame
}
