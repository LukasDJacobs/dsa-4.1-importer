import type {
  CharacterData,
  Talent,
  NormalTalent,
  CombatTalent,
  Attributes,
  Resources,
  CombatAttributes,
  Advantage,
  Disadvantage,
  LinguisticTalent,
  Spell,
  Test3d20,
  SpecialAbility,
  GenericItem,
  Liturgy,
  MeleeWeapon,
  RangedWeapon,
  Shield,
  Armor,
} from '../model/types'

import { Packs } from '../importer/packs'
import { advantages } from '../advantages'
import { disadvantages } from '../disadvantages'
import { spellNameMap } from './spell-map'

export function getHeldensoftwareCharacterData(
  xmlString: string,
  packs: Packs,
  importEquipment: boolean
): CharacterData {
  const data = {} as CharacterData
  const xml = $($.parseXML(xmlString))
  data.name = xml.find('held').attr('name') || ''
  data.attributes = getAttributes(xml)
  data.resources = computeResources(xml, data.attributes)
  data.combatAttributes = getCombatAttributes(xml, data.attributes)
  data.specialAbilities = getSpecialAbilities(xml, packs.specialAbilities)
  data.advantages = getAdvantages(xml)
  data.disadvantages = getDisadvantages(xml)
  data.talents = getTalents(xml, packs.talents) as NormalTalent[]
  data.combatTalents = getCombatTalents(
    xml,
    packs.combatTalents,
    data.combatAttributes
  )
  data.linguisticTalents = getLinguisticTalents(xml)
  data.spells = getSpells(xml, packs.spells)
  data.liturgies = getLiturgies(xml, packs.liturgies)

  if (importEquipment) {
    data.meleeWeapons = getMeleeWeapons(xml, packs.meleeWeapons)
    data.rangedWeapons = getRangedWeapons(xml, packs.rangedWeapons)
    data.armors = getArmors(xml, packs.armors)
    data.shields = getShields(xml, packs.shields)
    data.genericItems = getGenericItems(xml, packs)
  }

  return data
}

function getAttributes(xml: JQuery<XMLDocument>): Attributes {
  const attributeNames = {
    courage: 'Mut',
    cleverness: 'Klugheit',
    intuition: 'Intuition',
    charisma: 'Charisma',
    dexterity: 'Fingerfertigkeit',
    agility: 'Gewandtheit',
    constitution: 'Konstitution',
    strength: 'Körperkraft',
  }

  const attributes = {} as Attributes

  Object.entries(attributeNames).forEach(([key, value]) => {
    attributes[key] = parseInt(
      xml.find('eigenschaft[name="' + value + '"]').attr('value') || '' || ''
    )
    attributes[key] += parseInt(
      xml.find('eigenschaft[name="' + value + '"]').attr('mod') || '' || ''
    )
  })

  return attributes
}

type AttributeFormula = {
  attributes: string[]
  factor: number
}

function computeDerivedValue(
  formula: AttributeFormula,
  attributes: Attributes
) {
  let value = formula.attributes.reduce((current: number, next: string) => {
    return current + attributes[next]
  }, 0)
  value *= formula.factor
  value = Math.round(value)
  return value
}

function computeResources(
  xml: JQuery<XMLDocument>,
  attributes: Attributes
): Resources {
  const resources = {} as Resources

  const resourceNames = {
    vitality: 'Lebensenergie',
    endurance: 'Ausdauer',
    astralEnergy: 'Astralenergie',
    karmicEnergy: 'Karmaenergie',
  }

  const resourceFormulas = {
    vitality: {
      attributes: ['constitution', 'constitution', 'strength'],
      factor: 0.5,
    },
    endurance: {
      attributes: ['courage', 'constitution', 'agility'],
      factor: 0.5,
    },
    astralEnergy: {
      attributes: ['courage', 'intuition', 'charisma'],
      factor: 0.5,
    },
    karmicEnergy: {
      attributes: [],
      factor: 0,
    },
  }

  Object.entries(resourceNames).forEach(([key, name]) => {
    let value = computeDerivedValue(resourceFormulas[key], attributes)
    value += parseInt(
      xml.find('eigenschaft[name="' + name + '"]').attr('value') || '' || ''
    )
    value += parseInt(
      xml.find('eigenschaft[name="' + name + '"]').attr('mod') || '' || ''
    )
    resources[key] = {
      value: value,
      max: value,
    }
  })

  return resources
}

function getCombatAttributes(
  xml: JQuery<XMLDocument>,
  attributes: Attributes
): CombatAttributes {
  const combatAttributes = {} as CombatAttributes
  const combatAttributeNames = {
    baseInitiative: 'ini',
    baseAttack: 'at',
    baseParry: 'pa',
    baseRangedAttack: 'fk',
  }

  Object.entries(combatAttributeNames).forEach(([key, value]) => {
    combatAttributes[key] = parseInt(
      xml.find('eigenschaft[name="' + value + '"]').attr('value') || ''
    )
  })

  const magicResistanceFormula: AttributeFormula = {
    attributes: ['courage', 'cleverness', 'constitution'],
    factor: 0.2,
  }
  combatAttributes.magicResistance = computeDerivedValue(
    magicResistanceFormula,
    attributes
  )
  combatAttributes.magicResistance += parseInt(
    xml.find('eigenschaft[name="Magieresistenz"]').attr('value') || ''
  )
  combatAttributes.magicResistance += parseInt(
    xml.find('eigenschaft[name="Magieresistenz"]').attr('mod') || ''
  )
  return combatAttributes
}

function getSpecialAbilities(
  xml: JQuery<XMLDocument>,
  specialAbilityList: SpecialAbility[]
): SpecialAbility[] {
  return getItems<SpecialAbility>(
    xml,
    'sonderfertigkeit',
    new SpecialAbilityDataProvider(specialAbilityList)
  )
}

interface ItemDataProvider<ItemType> {
  get(entryData: HTMLElement): ItemType | undefined
}

function getItems<ItemType>(
  xml: JQuery<XMLDocument>,
  query: string,
  dataProvider: ItemDataProvider<ItemType>
): ItemType[] {
  const entries = xml.find(query).toArray()
  const items = [] as ItemType[]
  for (const entry of entries) {
    const item = dataProvider.get(entry)
    if (item) {
      items.push(item)
    }
  }
  return items.filter((item) => item)
}

function normalizeSpecialAbilityName(specialAbilityName) {
  return specialAbilityName?.toLowerCase().replace(':', '')
}

function equalSpecialAbility(specialAbility1, specialAbility2) {
  return (
    normalizeSpecialAbilityName(specialAbility1) ==
    normalizeSpecialAbilityName(specialAbility2)
  )
}

class SpecialAbilityDataProvider implements ItemDataProvider<SpecialAbility> {
  specialAbilityList: SpecialAbility[]

  constructor(specialAbilityList: SpecialAbility[]) {
    this.specialAbilityList = specialAbilityList
  }

  get(entryData: HTMLElement): SpecialAbility | undefined {
    const specialAbilityName = getSpecialAbilityName(entryData)

    if (isLiturgySpecialAbility(specialAbilityName)) return undefined

    const specialAbility = this.specialAbilityList.find((specialAbility) =>
      equalSpecialAbility(specialAbilityName, specialAbility.name)
    )
    if (specialAbility) {
      return {
        name: specialAbility.name,
        sid: specialAbility.sid,
      }
    }
    return {
      name: specialAbilityName,
      sid: '',
    }
  }
}

function getSpecialAbilityName(entry) {
  let specialAbilityName = $(entry).attr('name') || ''
  const subEntries = $(entry).find(':first-child')
  if (subEntries.length > 0) {
    const subEntryName = $(subEntries[0]).attr('name')
    specialAbilityName += ' (' + subEntryName + ')'
  }
  return specialAbilityName
}

function isLiturgySpecialAbility(name: string) {
  return name.startsWith('Liturgie: ')
}

function getAdvantages(xml: JQuery<XMLDocument>): Advantage[] {
  return getItems<Advantage>(xml, 'vorteil', new AdvantageDataProvider())
}

function getDisadvantages(xml: JQuery<XMLDocument>): Disadvantage[] {
  return getItems<Disadvantage>(xml, 'vorteil', new DisadvantageDataProvider())
}

function normalizeDisAdvantageName(name) {
  const normalizedName = name
    .split('[')[0]
    .split('bzgl.')[0]
    .split(' vor')[0]
    .toLowerCase()
    .trim()
    .replace('gutaussehend', 'gut aussehend')
  return normalizedName
}

function equalDisAdvantage(a, b) {
  return normalizeDisAdvantageName(a) === normalizeDisAdvantageName(b)
}

class DisadvantageDataProvider implements ItemDataProvider<Disadvantage> {
  get(entryData: HTMLElement): Disadvantage | undefined {
    const entryName = $(entryData).attr('name') || ''
    let entryValue = $(entryData).attr('value') || ''
    let itemData: Disadvantage | undefined = undefined
    const disadvantage = disadvantages.find((disadvantage) =>
      equalDisAdvantage(disadvantage.name, entryName)
    )
    if (disadvantage) {
      let disadvantageName = disadvantage.name.split('[')[0].trim()
      let additionalName
      if (!disadvantage.negativeAttribute) {
        if (entryValue && isNaN(+entryValue)) {
          additionalName = entryValue
        }
      } else {
        additionalName = entryName.split('vor ')[1]
      }
      disadvantageName +=
        additionalName != undefined && additionalName != ''
          ? ' ' + additionalName
          : ''
      const subEntries = $(entryData).find('auswahl')

      if (subEntries.length >= 2) {
        const len = subEntries.length
        const subEntryName = $(subEntries[len - 1]).attr('value') || ''
        if (disadvantageName == 'Schlechte Eigenschaft') {
          disadvantageName = subEntryName
        } else {
          disadvantageName += ' (' + subEntryName + ')'
        }
        entryValue = $(subEntries[len - 2]).attr('value') || ''
      }
      itemData = {
        name: disadvantageName,
        isNegativeAttribute: disadvantage.negativeAttribute || false,
        sid: '',
      }
      if (!isNaN(+entryValue)) {
        itemData.value = Number(entryValue)
      }
    }
    return itemData
  }
}

class AdvantageDataProvider implements ItemDataProvider<Advantage> {
  get(entry: HTMLElement): Advantage | undefined {
    const entryName = $(entry).attr('name')
    const entryValue = $(entry).attr('value') || ''
    let itemData: Advantage | undefined = undefined
    let advantage = advantages.find((advantage) =>
      equalDisAdvantage(advantage, entryName)
    )
    if (advantage) {
      if (entryValue && isNaN(+entryValue)) {
        advantage = advantage.split('[')[0] + ' ' + entryValue
      }
      itemData = {
        name: advantage,
        sid: '',
      }
      if (!isNaN(+entryValue)) {
        itemData.value = Number(entryValue)
      }
    }
    return itemData
  }
}

function getTalents(xml: JQuery<XMLDocument>, talentList: Talent[]): Talent[] {
  return getItems<Talent>(
    xml,
    'talentliste talent',
    new TalentDataProvider(talentList)
  )
}

function getCombatTalents(
  xml: JQuery<XMLDocument>,
  talentList: CombatTalent[],
  combatAttributes: CombatAttributes
): CombatTalent[] {
  return getItems<CombatTalent>(
    xml,
    'talentliste talent',
    new CombatTalentDataProvider(talentList, xml, combatAttributes)
  )
}

function getLinguisticTalents(xml: JQuery<XMLDocument>): LinguisticTalent[] {
  return getItems<LinguisticTalent>(
    xml,
    'talentliste talent',
    new LinguisticTalentDataProvider()
  )
}

function normalizeTalentName(talentName) {
  return talentName
    .toLowerCase()
    .replace(':', '')
    .replace('/', ' und ')
    .replace('fallen stellen', 'fallenstellen')
    .replace('geografie', 'geographie')
    .replace('zweihandhiebwaffen', 'zweihand-hiebwaffen')
    .replace('kartografie', 'kartographie')
}

function equalTalent(talent1, talent2) {
  return normalizeTalentName(talent1) == normalizeTalentName(talent2)
}

class TalentDataProvider implements ItemDataProvider<Talent> {
  talentList: Talent[]

  constructor(talentList: Talent[]) {
    this.talentList = talentList
  }

  get(entry: HTMLElement): Talent | undefined {
    const entryName = $(entry).attr('name')
    const talentValue = $(entry).attr('value') || ''
    const talent = this.talentList.find((talent) =>
      equalTalent(entryName, talent.name)
    )
    if (talent) {
      return {
        ...talent,
        value: Number(talentValue),
      }
    }
  }
}

class CombatTalentDataProvider implements ItemDataProvider<CombatTalent> {
  talentDataProver: TalentDataProvider
  xml: JQuery<XMLDocument>
  combatAttributes: CombatAttributes

  constructor(
    talentList: CombatTalent[],
    xml: JQuery<XMLDocument>,
    combatAttributes: CombatAttributes
  ) {
    this.talentDataProver = new TalentDataProvider(talentList)
    this.xml = xml
    this.combatAttributes = combatAttributes
  }

  get(entry: HTMLElement): CombatTalent {
    const talent = this.talentDataProver.get(entry) as CombatTalent
    if (
      talent &&
      (talent.combatType === 'melee' || talent.combatType === 'unarmed')
    ) {
      const entryName = $(entry).attr('name')
      const combatTalent = $(this.xml).find(`kampfwerte[name="${entryName}"]`)
      const attack = Number(combatTalent.find('attacke').attr('value') || '')
      const parry = Number(combatTalent.find('parade').attr('value') || '')
      return {
        ...talent,
        attack: attack - this.combatAttributes.baseAttack,
        parry: parry - this.combatAttributes.baseParry,
      }
    }
    if (talent && talent.combatType === 'ranged') {
      return {
        ...talent,
        rangedAttack: talent.value,
      }
    }
    if (talent && talent.combatType === 'special') {
      return {
        ...talent,
        attack: talent.value,
      }
    }
    return talent
  }
}

class LinguisticTalentDataProvider
  implements ItemDataProvider<LinguisticTalent> {
  get(entry: HTMLElement): LinguisticTalent | undefined {
    const talentName = $(entry).attr('name') || ''
    const talentValue = Number($(entry).attr('value') || '')
    for (const langSubName of ['Sprachen kennen', 'Lesen/Schreiben']) {
      if (talentName.includes(langSubName)) {
        const languageName = talentName.replace(langSubName, '').trim()
        const langType = {
          'Sprachen kennen': 'language',
          'Lesen/Schreiben': 'scripture',
        }[langSubName]
        const complexity = Number($(entry).attr('k'))
        return {
          type: 'linguistic',
          name: languageName,
          value: talentValue,
          linguisticType: langType,
          complexity: complexity,
        } as LinguisticTalent
      }
    }
  }
}

function equalSpell(spell1: string, spell2: Spell): boolean {
  return spellNameMap[spell1] === spell2.sid
}

class SpellDataProvider implements ItemDataProvider<Spell> {
  spellList: Spell[]

  constructor(spellList: Spell[]) {
    this.spellList = spellList
  }

  get(entry: HTMLElement): Spell {
    const spellName = $(entry).attr('name') || ''
    const spellValue = Number($(entry).attr('value') || '')

    const spell = this.spellList.find((spell) => equalSpell(spellName, spell))
    if (spell) {
      return {
        ...spell,
        value: Number(spellValue),
      } as Spell
    }

    const spellRoll = getSpellRoll(entry)
    return {
      name: spellName,
      type: 'spell',
      value: spellValue,
      test: spellRoll,
      lcdPage: undefined,
      astralCost: '',
      range: '',
      sid: '',
    }
  }
}

function getSpellRoll(entry: HTMLElement): Test3d20 {
  const attributeDict = {
    MU: 'courage',
    KL: 'cleverness',
    IN: 'intuition',
    CH: 'charisma',
    FF: 'dexterity',
    GE: 'agility',
    KO: 'constitution',
    KK: 'strength',
  }

  const spellRollString = $(entry).attr('probe') || ''
  const spellRoll = spellRollString
    .trim()
    .replace('(', '')
    .replace(')', '')
    .split('/')
    .map((attribute) => {
      return attributeDict[attribute]
    })
  return {
    firstAttribute: spellRoll[0],
    secondAttribute: spellRoll[1],
    thirdAttribute: spellRoll[2],
  }
}

function getSpells(xml: JQuery<XMLDocument>, spellList: Spell[]): Spell[] {
  return getItems<Spell>(
    xml,
    'zauberliste zauber',
    new SpellDataProvider(spellList)
  )
}

function getGenericDetails(entry: HTMLElement): GenericItem {
  let itemName = $(entry).attr('name') || ''
  const itemValue = parseInt($(entry).attr('anzahl') || '')
  let itemWeight: number | undefined
  let itemPrice: number | undefined

  const itemDetails = $(entry).find('modallgemein')
  if (itemDetails.length >= 0) {
    const itemDetailName = itemDetails.find('name').attr('value') || ''
    if (itemDetailName !== '') {
      itemName = itemDetailName
    }

    const itemWeightValue = itemDetails.find('gewicht').attr('value')
    if (itemWeightValue) {
      itemWeight = parseInt(itemWeightValue)
    }
    const itemPriceValue = itemDetails.find('preis').attr('value')
    if (itemPriceValue) {
      itemPrice = parseInt(itemPriceValue)
    }
  }

  return {
    name: itemName,
    value: itemValue,
    price: itemPrice,
    weight: itemWeight,
    sid: '',
  }
}

function getMeleeWeapons(
  xml: JQuery<XMLDocument>,
  meleeWeapons: MeleeWeapon[]
) {
  return getItems<MeleeWeapon>(
    xml,
    'gegenstände gegenstand',
    new MeleeWeaponDataProvider(meleeWeapons)
  )
}

class MeleeWeaponDataProvider implements ItemDataProvider<MeleeWeapon> {
  meleeWeaponList: MeleeWeapon[]

  constructor(meleeWeaponList: MeleeWeapon[]) {
    this.meleeWeaponList = meleeWeaponList
    return
  }

  get(entry: HTMLElement): MeleeWeapon | undefined {
    const itemName = $(entry).attr('name') || ''

    const meleeWeapon = this.meleeWeaponList.find((i) => i.name === itemName)
    if (!meleeWeapon) {
      return undefined
    }

    return this.getMeleeWeapon(entry, { ...meleeWeapon })
  }

  getMeleeWeapon(entry: HTMLElement, item: MeleeWeapon): MeleeWeapon {
    const genericDetails = getGenericDetails(entry)

    item.name = genericDetails.name
    item.price = genericDetails.price || item.price
    item.weight = genericDetails.weight || item.weight

    const itemDetails = $(entry).find('Nahkampfwaffe')
    if (itemDetails.length > 0) {
      const damageDetails = itemDetails.find('trefferpunkte')
      if (damageDetails.length > 0) {
        const diceCount = parseInt(damageDetails.attr('mul') || '1')
        const diceType = parseInt(damageDetails.attr('w') || '6')
        const damageBonus = parseInt(damageDetails.attr('sum') || '0')

        if (damageBonus >= 0) {
          item.damage = `${diceCount}d${diceType}+${damageBonus}`
        } else {
          item.damage = `${diceCount}d${diceType}-${damageBonus}`
        }
      }

      const strengthModDetails = itemDetails.find('tpkk')
      if (strengthModDetails.length > 0) {
        item.strengthMod.threshold = parseInt(
          strengthModDetails.attr('kk') || '20'
        )
        item.strengthMod.hitPointStep = parseInt(
          strengthModDetails.attr('schrittweite') || '20'
        )
      }

      const weaponModDetails = itemDetails.find('wm')
      if (weaponModDetails.length > 0) {
        item.weaponMod.attack = parseInt(weaponModDetails.attr('at') || '0')
        item.weaponMod.parry = parseInt(weaponModDetails.attr('pa') || '0')
      }

      const breakingFactor = itemDetails.find('bf')
      if (breakingFactor.length > 0) {
        item.breakingFactor = parseInt(breakingFactor.attr('akt') || '0')
      }

      const initiativeMod = itemDetails.find('inimod')
      if (initiativeMod.length > 0) {
        item.initiativeMod = parseInt(initiativeMod.attr('ini') || '0')
      }
    }

    return item
  }
}

function getRangedWeapons(
  xml: JQuery<XMLDocument>,
  rangedWeapons: RangedWeapon[]
) {
  return getItems<RangedWeapon>(
    xml,
    'gegenstände gegenstand',
    new RangedWeaponDataProvider(rangedWeapons)
  )
}

class RangedWeaponDataProvider implements ItemDataProvider<RangedWeapon> {
  rangedWeaponList: RangedWeapon[]

  constructor(rangedWeaponList: RangedWeapon[]) {
    this.rangedWeaponList = rangedWeaponList
    return
  }

  get(entry: HTMLElement): RangedWeapon | undefined {
    const itemName = $(entry).attr('name') || ''

    const rangedWeapon = this.rangedWeaponList.find((i) => i.name === itemName)
    if (!rangedWeapon) {
      return undefined
    }

    return this.getRangedWeapon(entry, { ...rangedWeapon })
  }

  getRangedWeapon(entry: HTMLElement, item: RangedWeapon): RangedWeapon {
    const genericDetails = getGenericDetails(entry)

    item.name = genericDetails.name
    item.price = genericDetails.price || item.price
    item.weight = genericDetails.weight || item.weight

    const itemDetails = $(entry).find('Fernkampfwaffe')
    if (itemDetails) {
      const damageDetails = itemDetails.find('trefferpunkte')
      if (damageDetails) {
        const diceCount = damageDetails.attr('mul') || 1
        const diceType = damageDetails.attr('w') || 6
        const damageBonus = damageDetails.attr('sum') || 0

        if (damageBonus >= 0) {
          item.damage = `${diceCount}d${diceType}+${damageBonus}`
        } else {
          item.damage = `${diceCount}d${diceType}-${damageBonus}`
        }
      }

      const loadtime = itemDetails.find('laden')
      if (loadtime.length > 0) {
        item.loadtime = parseInt(loadtime.attr('aktionen') || '0')
      }

      const projectileType = itemDetails.find('munition')
      if (projectileType.length > 0) {
        item.projectileType = projectileType.attr('art') || ''
      }

      const ranges = itemDetails.find('entfernung')
      if (ranges.length > 0) {
        item.ranges.veryClose = parseInt(ranges.attr('E0') || '0')
        item.ranges.close = parseInt(ranges.attr('E1') || '0')
        item.ranges.medium = parseInt(ranges.attr('E2') || '0')
        item.ranges.far = parseInt(ranges.attr('E3') || '0')
        item.ranges.veryFar = parseInt(ranges.attr('E4') || '0')
      }

      const bonusDamages = itemDetails.find('tpmod')
      if (bonusDamages.length > 0) {
        item.bonusDamages.veryClose = parseInt(bonusDamages.attr('M0') || '0')
        item.bonusDamages.close = parseInt(bonusDamages.attr('M1') || '0')
        item.bonusDamages.medium = parseInt(bonusDamages.attr('M2') || '0')
        item.bonusDamages.far = parseInt(bonusDamages.attr('M3') || '0')
        item.bonusDamages.veryFar = parseInt(bonusDamages.attr('M4') || '0')
      }
    }

    return item
  }
}

function getArmors(xml: JQuery<XMLDocument>, armors: Armor[]) {
  return getItems<Armor>(
    xml,
    'gegenstände gegenstand',
    new ArmorDataProvider(armors)
  )
}

class ArmorDataProvider implements ItemDataProvider<Armor> {
  armorList: Armor[]

  constructor(armorList: Armor[]) {
    this.armorList = armorList
    return
  }

  get(entry: HTMLElement): Armor | undefined {
    const itemName = $(entry).attr('name') || ''

    const armor = this.armorList.find((i) => i.name === itemName)
    if (!armor) {
      return undefined
    }

    return this.getArmor(entry, { ...armor })
  }

  getArmor(entry: HTMLElement, item: Armor): Armor {
    const genericDetails = getGenericDetails(entry)

    item.name = genericDetails.name
    item.price = genericDetails.price || item.price
    item.weight = genericDetails.weight || item.weight

    const itemDetails = $(entry).find('Rüstung')
    if (itemDetails.length > 0) {
      const armorClass = itemDetails.find('rs')
      if (armorClass) {
        item.armorClass = parseInt(armorClass.attr('value') || '0')
      }

      const encumbarance = itemDetails.find('gesbe')
      if (encumbarance.length > 0) {
        item.encumbarance = parseInt(encumbarance.attr('value') || '0')
        item.zonedEncumbrance = item.encumbarance
      }

      const headClass = itemDetails.find('kopf')
      if (headClass.length > 0) {
        item.zonedArmorClass.head = parseInt(headClass.attr('value') || '0')
      }

      const chestClass = itemDetails.find('brust')
      if (chestClass.length > 0) {
        item.zonedArmorClass.chest = parseInt(chestClass.attr('value') || '0')
      }

      const leftArmClass = itemDetails.find('linkerarm')
      if (leftArmClass.length > 0) {
        item.zonedArmorClass.leftArm = parseInt(
          leftArmClass.attr('value') || '0'
        )
      }

      const rightArmClass = itemDetails.find('rechterarm')
      if (rightArmClass.length > 0) {
        item.zonedArmorClass.rightArm = parseInt(
          rightArmClass.attr('value') || '0'
        )
      }

      const torsoClass = itemDetails.find('bauch')
      if (torsoClass.length > 0) {
        item.zonedArmorClass.torso = parseInt(torsoClass.attr('value') || '0')
      }

      const leftLegClass = itemDetails.find('linkesbein')
      if (leftLegClass.length > 0) {
        item.zonedArmorClass.leftLeg = parseInt(
          leftLegClass.attr('value') || '0'
        )
      }

      const rightLegClass = itemDetails.find('rechtesbein')
      if (rightLegClass.length > 0) {
        item.zonedArmorClass.rightLeg = parseInt(
          rightLegClass.attr('value') || '0'
        )
      }

      const backClass = itemDetails.find('ruecken')
      if (backClass.length > 0) {
        item.zonedArmorClass.back = parseInt(backClass.attr('value') || '0')
      }
    }

    return item
  }
}

function getShields(xml: JQuery<XMLDocument>, shields: Shield[]) {
  return getItems<Shield>(
    xml,
    'gegenstände gegenstand',
    new ShieldDataProvider(shields)
  )
}

class ShieldDataProvider implements ItemDataProvider<Shield> {
  shieldList: Shield[]

  constructor(shieldList: Shield[]) {
    this.shieldList = shieldList
    return
  }

  get(entry: HTMLElement): Shield | undefined {
    const itemName = $(entry).attr('name') || ''

    const shield = this.shieldList.find((i) => i.name === itemName)
    if (!shield) {
      return undefined
    }

    return this.getShield(entry, { ...shield })
  }

  getShield(entry: HTMLElement, item: Shield): Shield {
    const genericDetails = getGenericDetails(entry)

    item.name = genericDetails.name
    item.price = genericDetails.price || item.price
    item.weight = genericDetails.weight || item.weight

    const itemDetails = $(entry).find('Schild')
    if (itemDetails) {
      const weaponModDetails = itemDetails.find('wm')
      if (weaponModDetails.length > 0) {
        item.weaponMod.attack = parseInt(weaponModDetails.attr('at') || '0')
        item.weaponMod.parry = parseInt(weaponModDetails.attr('pa') || '0')
      }

      const breakingFactor = itemDetails.find('bf')
      if (breakingFactor.length > 0) {
        item.breakingFactor = parseInt(breakingFactor.attr('akt') || '0')
      }

      const initiativeMod = itemDetails.find('inimod')
      if (initiativeMod.length > 0) {
        item.initiativeMod = parseInt(initiativeMod.attr('ini') || '0')
      }
    }

    return item
  }
}

function getGenericItems(xml: JQuery<XMLDocument>, packs: Packs) {
  return getItems<GenericItem>(
    xml,
    'gegenstände gegenstand',
    new GenericItemDataProvider(packs)
  )
}

class GenericItemDataProvider implements ItemDataProvider<GenericItem> {
  packs: Packs

  constructor(packs: Packs) {
    this.packs = packs
  }

  get(entry: HTMLElement): GenericItem | undefined {
    const itemName = $(entry).attr('name') || ''

    const meleeWeapon = this.packs.meleeWeapons.find((i) => i.name === itemName)
    if (meleeWeapon) {
      return undefined
    }

    const rangedWeapon = this.packs.rangedWeapons.find(
      (i) => i.name === itemName
    )
    if (rangedWeapon) {
      return undefined
    }

    const shield = this.packs.shields.find((i) => i.name === itemName)
    if (shield) {
      return undefined
    }

    const armor = this.packs.armors.find((i) => i.name === itemName)
    if (armor) {
      return undefined
    }

    const genericDetails = getGenericDetails(entry)

    if (!genericDetails.value) {
      genericDetails.value = 1
    }
    if (!genericDetails.price) {
      genericDetails.price = 0
    }
    if (!genericDetails.weight) {
      genericDetails.weight = 0
    }

    return genericDetails
  }
}

function getLiturgies(
  xml: JQuery<XMLDocument>,
  liturgyList: Liturgy[]
): Liturgy[] {
  return getItems<Liturgy>(
    xml,
    'sonderfertigkeit',
    new LiturgyDataProvider(liturgyList)
  )
}

class LiturgyDataProvider implements ItemDataProvider<Liturgy> {
  liturgyList: Liturgy[]

  constructor(liturgyList: Liturgy[]) {
    this.liturgyList = liturgyList
  }

  get(entryData: HTMLElement): Liturgy | undefined {
    const liturgyName = getSpecialAbilityName(entryData)

    if (!isLiturgySpecialAbility(liturgyName)) return undefined

    const liturgyNameAndCustomDegree = this.getLiturgyNameAndCustomDegree(
      liturgyName
    )
    const liturgyAbility = this.liturgyList.find(
      (liturgy) => liturgyNameAndCustomDegree.name === liturgy.name
    )

    if (liturgyAbility) {
      return {
        name: liturgyAbility.name,
        type: 'liturgy',
        sid: liturgyAbility.sid,
        description: liturgyAbility.description,
        castTime: liturgyAbility.castTime,
        effectTime: liturgyAbility.effectTime,
        targetClasses: liturgyAbility.targetClasses,
        range: liturgyAbility.range,
        variants: liturgyAbility.variants,
        degree: liturgyNameAndCustomDegree.customDegree
          ? liturgyNameAndCustomDegree.customDegree
          : liturgyAbility.degree,
        castType: liturgyAbility.castType,
        llPage: liturgyAbility.llPage,
      }
    }
    return {
      name: liturgyNameAndCustomDegree.name,
      type: 'liturgy',
      sid: '',
      description: '',
      castTime: { duration: 0, unit: '', info: '' },
      effectTime: { duration: 0, unit: '', info: '' },
      targetClasses: [],
      range: '',
      variants: [],
      degree: liturgyNameAndCustomDegree.customDegree
        ? liturgyNameAndCustomDegree.customDegree
        : '',
      castType: '',
      llPage: 0,
    }
  }

  getLiturgyNameAndCustomDegree(
    liturgyName: string
  ): {
    name: string
    customDegree: string | undefined
  } {
    const degreeMap = new Map<string, string>([
      ['(I)', 'I'],
      ['(II)', 'II'],
      ['(III)', 'III'],
      ['(IV)', 'IV'],
      ['(V)', 'V'],
      ['(VI)', 'VI'],
      ['(VII)', 'VII'],
      ['(VIII)', 'VIII'],
      ['(IX)', 'IX'],
      ['(X)', 'X'],
    ])

    let customDegree: string | undefined = undefined
    for (const keyValue of degreeMap) {
      if (liturgyName.includes(keyValue[0])) {
        customDegree = keyValue[1]
        liturgyName = liturgyName.replace(keyValue[0], '')
        break
      }
    }

    liturgyName = liturgyName.replace('Liturgie: ', '').trim()
    return { name: liturgyName, customDegree: customDegree }
  }
}
