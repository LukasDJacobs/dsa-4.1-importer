import {
  Armor,
  CombatTalent,
  Liturgy,
  MeleeWeapon,
  RangedWeapon,
  Shield,
  SpecialAbility,
  Spell,
  Talent,
  PackNames,
  CompediumTypes,
} from '../model/types'

import { getGame } from '../utils'

export type Packs = {
  talents: Talent[]
  spells: Spell[]
  combatTalents: CombatTalent[]
  specialAbilities: SpecialAbility[]
  liturgies: Liturgy[]
  meleeWeapons: MeleeWeapon[]
  rangedWeapons: RangedWeapon[]
  shields: Shield[]
  armors: Armor[]
}

export async function getPacks(): Promise<Packs> {
  return {
    talents: await getTalentList(),
    spells: await getSpellList(),
    combatTalents: await getCombatTalentList(),
    specialAbilities: await getSpecialAbilityList(),
    liturgies: await getLiturgyList(),
    meleeWeapons: await getMeleeWeaponList(),
    rangedWeapons: await getRangedWeaponList(),
    shields: await getShieldList(),
    armors: await getArmorList(),
  }
}

async function getCompendiumPackFromSettings(
  packName: PackNames
): Promise<CompediumTypes[]> {
  const game = getGame()

  const compendium = game.settings.get('dsa-41-importer', packName) as string
  return (await game.packs.get(compendium)?.getDocuments()) || []
}

async function getTalentList(): Promise<Talent[]> {
  const talentPack = await getCompendiumPackFromSettings('talent-pack')

  return talentPack.map((talent) => ({
    type: talent.data.data.type,
    name: talent.data.name,
    category: talent.data.data.category,
    test: {
      firstAttribute: talent.data.data.test.firstAttribute,
      secondAttribute: talent.data.data.test.secondAttribute,
      thirdAttribute: talent.data.data.test.thirdAttribute,
    },
    sid: talent.data.data.sid,
    linguisticType: ['scripture', 'language'].includes(talent.data.data.type)
      ? talent.data.data.type
      : undefined,
    complexity: talent.data.data.complexity,
    value: 0,
    effectiveEncumbarance: talent.data.data.effectiveEncumbarance,
  }))
}

async function getSpellList(): Promise<Spell[]> {
  const spellPack = await getCompendiumPackFromSettings('spell-pack')

  return spellPack.map((spell) => ({
    name: spell.data.name,
    test: {
      firstAttribute: spell.data.data.test.firstAttribute,
      secondAttribute: spell.data.data.test.secondAttribute,
      thirdAttribute: spell.data.data.test.thirdAttribute,
    },
    sid: spell.data.data.sid,
    astralCost: spell.data.data.astralCost,
    range: spell.data.data.range,
    lcdPage: spell.data.data.lcdPage,
    value: 0,
    type: 'spell',
  }))
}

async function getCombatTalentList(): Promise<CombatTalent[]> {
  const talentPack = await getCompendiumPackFromSettings('combattalent-pack')

  return talentPack.map((talent) => ({
    attack: 0,
    category: talent.data.data.category,
    value: 0,
    type: talent.data.data.type,
    name: talent.data.name,
    combatType: talent.data.data.combat.category,
    sid: talent.data.data.sid,
    effectiveEncumbarance: talent.data.data.effectiveEncumbarance,
  }))
}

async function getSpecialAbilityList(): Promise<SpecialAbility[]> {
  const specialAbilityPack = await getCompendiumPackFromSettings(
    'specialability-pack'
  )

  return specialAbilityPack.map(
    (specialAbility) =>
      ({
        name: specialAbility.data.name,
        sid: specialAbility.data.data.sid,
      } as SpecialAbility)
  )
}

async function getLiturgyList(): Promise<Liturgy[]> {
  const liturgyPack = await getCompendiumPackFromSettings('liturgy-pack')

  return liturgyPack.map(
    (liturgy) =>
      ({
        name: liturgy.data.name,
        sid: liturgy.data.data.sid,
        description: liturgy.data.data.description,
        castTime: {
          duration: liturgy.data.data.castTime.duration,
          unit: liturgy.data.data.castTime.unit,
          info: liturgy.data.data.castTime.info,
        },
        effectTime: {
          duration: liturgy.data.data.effectTime.duration,
          unit: liturgy.data.data.effectTime.unit,
          info: liturgy.data.data.effectTime.info,
        },
        targetClasses: liturgy.data.data.targetClasses,
        range: liturgy.data.data.range,
        variants: liturgy.data.data.variants,
        degree: liturgy.data.data.degree,
        castType: liturgy.data.data.castType,
        llPage: liturgy.data.data.llPage,
      } as Liturgy)
  )
}

async function getMeleeWeaponList(): Promise<MeleeWeapon[]> {
  const meleeWeaponPack = await getCompendiumPackFromSettings(
    'meleeweapon-pack'
  )

  return meleeWeaponPack.map(
    (weapon) =>
      ({
        name: weapon.data.name,
        type: 'meleeWeapon',
        sid: weapon.data.data.sid,
        damage: weapon.data.data.damage,
        talent: weapon.data.data.talent,
        strengthMod: {
          threshold: weapon.data.data.strengthMod.threshold,
          hitPointStep: weapon.data.data.strengthMod.hitPointStep,
        },
        initiativeMod: weapon.data.data.initiativeMod,
        weaponMod: {
          attack: weapon.data.data.weaponMod.attack,
          parry: weapon.data.data.weaponMod.parry,
        },
        distanceClass: weapon.data.data.distanceClass,
        breakingFactor: weapon.data.data.breakingFactor,
        length: weapon.data.data.length,
        twoHanded: weapon.data.data.twoHanded,
        priviledged: weapon.data.data.priviledged,
        improvised: weapon.data.data.improvised,
        enduranceDamage: weapon.data.data.enduranceDamage,
        description: weapon.data.data.description,
        price: weapon.data.data.price,
        weight: weapon.data.data.weight,
        aaPage: weapon.data.data.aaPage,
        class: 'weapon',
      } as MeleeWeapon)
  )
}

async function getRangedWeaponList(): Promise<RangedWeapon[]> {
  const rangedWeaponPack = await getCompendiumPackFromSettings(
    'rangedweapon-pack'
  )

  return rangedWeaponPack.map(
    (weapon) =>
      ({
        name: weapon.data.name,
        type: 'rangedWeapon',
        sid: weapon.data.data.sid,
        damage: weapon.data.data.damage,
        talent: weapon.data.data.talent,
        ranges: {
          veryClose: weapon.data.data.ranges.veryClose,
          close: weapon.data.data.ranges.close,
          medium: weapon.data.data.ranges.medium,
          far: weapon.data.data.ranges.far,
          veryFar: weapon.data.data.ranges.veryFar,
        },
        bonusDamages: {
          veryClose: weapon.data.data.bonusDamages.veryClose,
          close: weapon.data.data.bonusDamages.close,
          medium: weapon.data.data.bonusDamages.medium,
          far: weapon.data.data.bonusDamages.far,
          veryFar: weapon.data.data.bonusDamages.veryFar,
        },
        loadtime: weapon.data.data.loadtime,
        projectileType: weapon.data.data.projectileType,
        projectilePrice: weapon.data.data.projectilePrice,
        projectileWeight: weapon.data.data.projectileWeight,
        loweredWoundThreshold: weapon.data.data.loweredWoundThreshold,
        entangles: weapon.data.data.entangles,
        improvised: weapon.data.data.improvised,
        enduranceDamage: weapon.data.data.enduranceDamage,
        description: weapon.data.data.description,
        price: weapon.data.data.price,
        weight: weapon.data.data.weight,
        aaPage: weapon.data.data.aaPage,
        class: 'weapon',
      } as RangedWeapon)
  )
}

async function getShieldList(): Promise<Shield[]> {
  const shieldPack = await getCompendiumPackFromSettings('shield-pack')

  return shieldPack.map(
    (shield) =>
      ({
        name: shield.data.name,
        type: 'shield',
        sid: shield.data.data.sid,
        initiativeMod: shield.data.data.damage,
        weaponMod: {
          attack: shield.data.data.weaponMod.attack,
          parry: shield.data.data.weaponMod.parry,
        },
        breakingFactor: shield.data.data.breakingFactor,
        description: shield.data.data.description,
        price: shield.data.data.price,
        weight: shield.data.data.weight,
        aaPage: shield.data.data.aaPage,
      } as Shield)
  )
}

async function getArmorList(): Promise<Armor[]> {
  const armorPack = await getCompendiumPackFromSettings('armor-pack')

  return armorPack.map(
    (armor) =>
      ({
        name: armor.data.name,
        type: 'armor',
        sid: armor.data.data.sid,
        armorClass: armor.data.data.armorClass,
        encumbarance: armor.data.data.encumbarance,
        zonedArmorClass: {
          head: armor.data.data.zonedArmorClass.head,
          chest: armor.data.data.zonedArmorClass.chest,
          torso: armor.data.data.zonedArmorClass.torso,
          back: armor.data.data.zonedArmorClass.back,
          leftArm: armor.data.data.zonedArmorClass.leftArm,
          rightArm: armor.data.data.zonedArmorClass.rightArm,
          leftLeg: armor.data.data.zonedArmorClass.leftLeg,
          rightLeg: armor.data.data.zonedArmorClass.rightLeg,
          tail: armor.data.data.zonedArmorClass.tail,
          total: armor.data.data.zonedArmorClass.total,
        },
        zonedEncumbrance: armor.data.data.zonedEncumbrance,
        description: armor.data.data.description,
        price: armor.data.data.price,
        weight: armor.data.data.weight,
        aaPage: armor.data.data.aaPage,
      } as Armor)
  )
}
