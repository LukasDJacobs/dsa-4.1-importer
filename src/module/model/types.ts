export type Attributes = {
  courage: number
  cleverness: number
  intuition: number
  charisma: number
  dexterity: number
  agility: number
  constitution: number
  strength: number
}

export type Resource = {
  value: number
  max: number
}

export type Resources = {
  vitality: Resource
  endurance: Resource
  astralEnergy: Resource
  karmicEnergy: Resource
}

export type CombatAttributes = {
  baseAttack: number
  baseParry: number
  baseRangedAttack: number
  baseInitiative: number
  magicResistance: number
}

export type BaseProperty = {
  sid: string
}

export type SpecialAbility = {
  name: string
} & BaseProperty

export type Advantage = {
  name: string
  value?: number
} & BaseProperty

export type Disadvantage = {
  name: string
  value?: number
  isNegativeAttribute: boolean
} & BaseProperty

export type GenericItem = {
  name: string
  value?: number
  weight?: number
  price?: number
} & BaseProperty

export type ItemClass = 'weapon' | 'shield'
export type WeaponType = 'meleeWeapon' | 'rangedWeapon'

export type EquipableItem = {
  class: ItemClass
  description: string
  price: number
  weight: number
  aaPage: string
} & BaseProperty

export type Weapon = {
  class: 'weapon'
  type: WeaponType
  name: string
  talent: string
  damage: string
  improvised: boolean
  enduranceDamage: boolean
} & EquipableItem

export type MeleeWeapon = {
  type: 'meleeWeapon'
  initiativeMod: number
  weaponMod: { attack: number; parry: number }
  strengthMod: { threshold: number; hitPointStep: number }
  distanceClass: string
  breakingFactor: number
  length: number
  twoHanded: boolean
  priviledged: boolean
} & Weapon

export type RangedWeapon = {
  type: 'rangedWeapon'
  ranges: {
    veryClose: number
    close: number
    medium: number
    far: number
    veryFar: number
  }
  bonusDamages: {
    veryClose: number
    close: number
    medium: number
    far: number
    veryFar: number
  }
  loadtime: number
  projectileType: string
  projectilePrice: number
  projectileWeight: number
  loweredWoundThreshold: boolean
  entangles: boolean
} & Weapon

export type Shield = {
  class: 'shield'
  type: 'shield'
  name: string
  initiativeMod: number
  weaponMod: { attack: number; parry: number }
  breakingFactor: number
} & EquipableItem

export type Armor = {
  name: string
  type: 'armor'
  armorClass: number
  encumbarance: number
  zonedArmorClass: {
    head: number
    chest: number
    leftArm: number
    rightArm: number
    torso: number
    leftLeg: number
    rightLeg: number
    back: number
    total: number
    tail: number
  }
  zonedEncumbrance: number
  description: string
  price: number
  weight: number
  aaPage: string
}

export type Test3d20 = {
  firstAttribute: string
  secondAttribute: string
  thirdAttribute: string
}

type EffectiveEncumbaranceType = 'none' | 'special' | 'formula'

type EffectiveEncumbarance = {
  type: EffectiveEncumbaranceType
  formula?: string
}

export type BaseTalent = {
  name: string
  category: string
  value: number
  effectiveEncumbarance: EffectiveEncumbarance
} & BaseProperty

export type Testable = {
  test?: Test3d20
}

export type NormalTalent = {
  type: 'normal'
} & BaseTalent &
  Testable

export type BaseCombatTalent = {
  type: 'combat'
} & BaseTalent

export type MeleeCombatTalent = {
  combatType: 'melee'
  attack: number
  parry: number
} & BaseCombatTalent

export type UnarmedCombatTalent = {
  combatType: 'unarmed'
  attack: number
  parry: number
} & BaseCombatTalent

export type RangedCombatTalent = {
  combatType: 'ranged'
  rangedAttack: number
} & BaseCombatTalent

export type SpecialCombatTalent = {
  combatType: 'special'
  attack: number
} & BaseCombatTalent

export type CombatTalent =
  | MeleeCombatTalent
  | UnarmedCombatTalent
  | RangedCombatTalent
  | SpecialCombatTalent

export type LinguisticTalentType = 'language' | 'scripture'

export type LinguisticTalent = {
  type: 'linguistic'
  linguisticType: LinguisticTalentType
  complexity: number
} & BaseTalent &
  Testable

export type Talent = NormalTalent | CombatTalent | LinguisticTalent

export type Spell = {
  name: string
  lcdPage: number | undefined
  astralCost: string
  range: string
  value: number
  type: 'spell'
} & Testable &
  BaseProperty

export type Liturgy = {
  name: string
  type: 'liturgy'
  description: string
  castTime: { duration: number; unit: string; info: string }
  effectTime: { duration: number; unit: string; info: string }
  targetClasses: string[]
  range: string
  variants: any[]
  degree: string
  castType: string
  llPage: number
} & BaseProperty

export type CharacterData = {
  name: string
  attributes: Attributes
  resources: Resources
  combatAttributes: CombatAttributes
  specialAbilities: SpecialAbility[]
  advantages: Advantage[]
  disadvantages: Disadvantage[]
  talents: NormalTalent[]
  combatTalents: CombatTalent[]
  linguisticTalents: LinguisticTalent[]
  spells: Spell[]
  liturgies: Liturgy[]
  genericItems: GenericItem[]
  meleeWeapons: MeleeWeapon[]
  rangedWeapons: RangedWeapon[]
  shields: Shield[]
  armors: Armor[]
}

export type PackNames =
  | 'talent-pack'
  | 'combattalent-pack'
  | 'specialability-pack'
  | 'meleeweapon-pack'
  | 'rangedweapon-pack'
  | 'shield-pack'
  | 'armor-pack'
  | 'spell-pack'
  | 'liturgy-pack'

export type CompediumTypes =
  | Actor
  | Item
  | JournalEntry
  | Macro
  | Playlist
  | RollTable
  | Scene
