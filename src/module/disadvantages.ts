export const disadvantages = [
  {
    name: 'Aberglaube',
    negativeAttribute: true,
  },
  {
    name: 'Albino',
  },
  {
    name: 'Angst vor [...]',
    negativeAttribute: true,
  },
  {
    name: 'Animalische Magie',
  },
  {
    name: 'Arkanophobie',
    negativeAttribute: true,
  },
  {
    name: 'Arroganz',
    negativeAttribute: true,
  },
  {
    name: 'Artefaktgebunden',
  },
  {
    name: 'Astraler Block',
  },
  {
    name: 'Autoritätsgläubig',
    negativeAttribute: true,
  },
  {
    name: 'Behäbig',
  },
  {
    name: 'Blutdurst',
    negativeAttribute: true,
  },
  {
    name: 'Blutrausch',
  },
  {
    name: 'Brünstigkeit',
    negativeAttribute: true,
  },
  {
    name: 'Dunkelangst',
    negativeAttribute: true,
  },
  {
    name: 'Einarmig',
  },
  {
    name: 'Gelähmter Arm',
  },
  {
    name: 'Einäugig',
  },
  {
    name: 'Einbeinig',
  },
  {
    name: 'Einbildungen',
  },
  {
    name: 'Eingeschränkte Elementarnähe',
  },
  {
    name: 'Eingeschränkter Sinn',
  },
  {
    name: 'Einhändig',
  },
  {
    name: 'Eitelkeit',
    negativeAttribute: true,
  },
  {
    name: 'Elfische Weltsicht',
  },
  {
    name: 'Farbenblind',
  },
  {
    name: 'Feind',
  },
  {
    name: 'Feste Gewohnheit',
  },
  {
    name: 'Festgefügtes Denken',
  },
  {
    name: 'Fettleibig',
  },
  {
    name: 'Fluch der Finsternis',
  },
  {
    name: 'Geiz',
    negativeAttribute: true,
  },
  {
    name: 'Gerechtigkeitswahn',
    negativeAttribute: true,
  },
  {
    name: 'Gesucht',
  },
  {
    name: 'Glasknochen',
  },
  {
    name: 'Goldgier',
    negativeAttribute: true,
  },
  {
    name: 'Größenwahn',
    negativeAttribute: true,
  },
  {
    name: 'Heimwehkrank',
  },
  {
    name: 'Hitzeempfindlichkeit',
  },
  {
    name: 'Höhenangst',
    negativeAttribute: true,
  },
  {
    name: 'Impulsiv',
  },
  {
    name: 'Jähzorn',
    negativeAttribute: true,
  },
  {
    name: 'Kälteempfindlichkeit',
  },
  {
    name: 'Kältestarre',
  },
  {
    name: 'Kein Vertrauter',
  },
  {
    name: 'Kristallgebunden',
  },
  {
    name: 'Kleinwüchsig',
  },
  {
    name: 'Körpergebundene Kraft',
  },
  {
    name: 'Krankhafte Reinlichkeit',
    negativeAttribute: true,
  },
  {
    name: 'Krankheitsanfällig',
  },
  {
    name: 'Kurzatmig',
  },
  {
    name: 'Lahm',
  },
  {
    name: 'Lästige Mindergeister',
  },
  {
    name: 'Lichtempfindlich',
  },
  {
    name: 'Lichtscheu',
  },
  {
    name: 'Madas Fluch',
  },
  {
    name: 'Medium',
  },
  {
    name: 'Meeresangst',
    negativeAttribute: true,
  },
  {
    name: 'Miserable Eigenschaft',
  },
  {
    name: 'Mondsüchtig',
  },
  {
    name: 'Moralkodex [Kirche]',
  },
  {
    name: 'Nachtblind',
  },
  {
    name: 'Nahrungsrestriktion',
  },
  {
    name: 'Neid',
    negativeAttribute: true,
  },
  {
    name: 'Neugier',
    negativeAttribute: true,
  },
  {
    name: 'Niedrige Astralkraft',
  },
  {
    name: 'Niedrige Lebenskraft',
  },
  {
    name: 'Niedrige Magieresistenz',
  },
  {
    name: 'Pechmagnet',
  },
  {
    name: 'Platzangst',
    negativeAttribute: true,
  },
  {
    name: 'Prinzipientreue',
  },
  {
    name: 'Rachsucht',
    negativeAttribute: true,
  },
  {
    name: 'Randgruppe',
  },
  {
    name: 'Raubtiergeruch',
  },
  {
    name: 'Raumangst',
    negativeAttribute: true,
  },
  {
    name: 'Rückschlag',
  },
  {
    name: 'Schlafstörungen',
  },
  {
    name: 'Schlafwandler',
  },
  {
    name: 'Schlechte Eigenschaft',
    negativeAttribute: true,
  },
  {
    name: 'Schlechte Regeneration',
  },
  {
    name: 'Schlechter Ruf',
  },
  {
    name: 'Schneller Alternd',
  },
  {
    name: 'Schulden',
  },
  {
    name: 'Schwache Ausstrahlung',
  },
  {
    name: 'Schwacher Astralkörper',
  },
  {
    name: 'Schwanzlos',
  },
  {
    name: 'Seffer Manich',
  },
  {
    name: 'Selbstgespräche',
  },
  {
    name: 'Sensibler Geruchssinn',
    negativeAttribute: true,
  },
  {
    name: 'Sippenlosigkeit',
  },
  {
    name: 'Sonnensucht',
    negativeAttribute: true,
  },
  {
    name: 'Speisegebote',
  },
  {
    name: 'Spielsucht',
    negativeAttribute: true,
  },
  {
    name: 'Sprachfehler',
  },
  {
    name: 'Spruchhemmung',
  },
  {
    name: 'Stigma',
  },
  {
    name: 'Streitsucht',
    negativeAttribute: true,
  },
  {
    name: 'Stubenhocker',
  },
  {
    name: 'Sucht',
  },
  {
    name: 'Thesisgebunden',
  },
  {
    name: 'Tollpatsch',
  },
  {
    name: 'Totenangst',
    negativeAttribute: true,
  },
  {
    name: 'Übler Geruch',
  },
  {
    name: 'Unangenehme Stimme',
  },
  {
    name: 'Unansehnlich',
  },
  {
    name: 'Unfähigkeit für [Talentgruppe]',
  },
  {
    name: 'Unfähigkeit für [Talent]',
  },
  {
    name: 'Unfrei',
  },
  {
    name: 'Ungebildet',
  },
  {
    name: 'Unstet',
  },
  {
    name: 'Unverträglichkeit mit verarbeitetem Metall',
  },
  {
    name: 'Vergesslichkeit',
  },
  {
    name: 'Verpflichtungen',
  },
  {
    name: 'Verschwendungssucht',
    negativeAttribute: true,
  },
  {
    name: 'Verwöhnt',
    negativeAttribute: true,
  },
  {
    name: 'Vorurteile gegen',
    negativeAttribute: true,
  },
  {
    name: 'Wahnvorstellungen',
  },
  {
    name: 'Wahrer Name',
  },
  {
    name: 'Weltfremd',
    negativeAttribute: true,
  },
  {
    name: 'Widerwärtiges Aussehen',
  },
  {
    name: 'Wilde Magie',
  },
  {
    name: 'Zielschwierigkeiten',
  },
  {
    name: 'Zögerlicher Zauberer',
  },
  {
    name: 'Zwergenwuchs',
  },
]
